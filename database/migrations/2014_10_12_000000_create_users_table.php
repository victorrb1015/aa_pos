<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles',function (Blueprint $t){
            $t->id();
            $t->string('name');
            $t->timestamps();
            $t->softDeletes();
        });

        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->foreignId('profile_id')
                ->constrained('profiles')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('modules', function (Blueprint $t){
            $t->id();
            $t->foreignId('profile_id')
                ->constrained('profiles')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $t->string('route');
            $t->timestamps();
            $t->softDeletes();
        });

        Schema::create('colors',function (Blueprint $t){
            $t->id();
            $t->string('name');
            $t->string('color_hex');
            $t->timestamps();
            $t->softDeletes();
        });

        Schema::create('dealers', function (Blueprint $t){
            $t->id();
            $t->string('name');
            $t->string('state');
            $t->string('phone');
            $t->string('logo');
            $t->string('color_header');
            $t->string('color_text');
            $t->timestamps();
            $t->softDeletes();
        });

        Schema::create('clients',function(Blueprint $t){
            $t->id();
            $t->string('name');
            $t->string('address');
            $t->string('city');
            $t->string('state');
            $t->integer('zipcode');
            $t->string('phone');
            $t->string('email');
            $t->string('installation_site');
            $t->timestamps();
            $t->softDeletes();
        });

        schema::create('components', function (Blueprint $t){
            $t->id();
            $t->string('name');
            $t->string('description');
            $t->decimal('price');
            $t->timestamps();
            $t->softDeletes();
        });

        Schema::create('back_walls', function (Blueprint $t){
            $t->id();
            $t->string('name');
            $t->timestamps();
            $t->softDeletes();
        });

        Schema::create('side_walls', function (Blueprint $t){
            $t->id();
            $t->string('name');
            $t->timestamps();
            $t->softDeletes();
        });

        Schema::create('fronts_walls', function (Blueprint $t){
            $t->id();
            $t->string('name');
            $t->timestamps();
            $t->softDeletes();
        });

        Schema::create('building_types', function (Blueprint $t){
            $t->id();
            $t->string('name');
            $t->boolean('style');
            $t->timestamps();
            $t->softDeletes();
        });

        Schema::create('east_enclosed', function (Blueprint $t){
            $t->id();
            $t->string('name');
            $t->timestamps();
            $t->softDeletes();
        });

        Schema::create('west_enclosed', function (Blueprint $t){
            $t->id();
            $t->string('name');
            $t->timestamps();
            $t->softDeletes();
        });

        Schema::create('orders', function (Blueprint $t){
            $t->id();
            $t->foreignId('employee_id')
                ->constrained('employees')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $t->foreignId('client_id')
                ->constrained('clients')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $t->foreignId('dealer_id')
                ->constrained('dealers')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $t->foreignId('building_type_id')
                ->constrained('building_types')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $t->foreignId('front_wall_id')
                ->constrained('fronts_walls')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $t->foreignId('side_wall_id')
                ->constrained('side_walls')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $t->foreignId('back_wall_id')
                ->constrained('back_walls')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $t->integer('invoice');
            $t->date('order_date');
            $t->date('installed_day');
            $t->string('color_roof');
            $t->string('color_walls');
            $t->string('color_trim');
            $t->string('surface_level');
            $t->integer('width');
            $t->integer('length_roof');
            $t->integer('length_frame');
            $t->integer('height');
            $t->decimal('total_sale');
            $t->decimal('deposit_percentage');
            $t->decimal('deposit');
            $t->string('cus_sig')->nullable();
            $t->date('cus_sig_date')->nullable();
            $t->string('cus_sig_2')->nullable();
            $t->string('installer_sig')->nullable();
            $t->date('sig_date')->nullable();
            $t->boolean('wainscot')->default(FALSE);
            $t->boolean('west_lean_to')->default(FALSE);
            $t->boolean('east_lean_to')->default(FALSE);
            $t->integer('west_len')->default(0);
            $t->integer('east_len')->default(0);
            $t->set('state', ['Quote', 'Order']);
            $t->timestamps();
            $t->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('building_types');
        Schema::dropIfExists('fronts_walls');
        Schema::dropIfExists('side_walls');
        Schema::dropIfExists('back_walls');
        Schema::dropIfExists('components');
        Schema::dropIfExists('clients');
        Schema::dropIfExists('dealers');
        Schema::dropIfExists('colors');
        Schema::dropIfExists('modules');
        Schema::dropIfExists('employees');
        Schema::dropIfExists('profiles');
    }
}
